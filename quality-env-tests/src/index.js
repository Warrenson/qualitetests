
// export default my_alpha_number_t;

// Description : Fonction my_alpha_number_t
// it 1 : Je souhaite tester si la fonction my_alpha_number_t renvoi bien “Hello” (execpt.toBe(“hello”))
// it 2 : Je souhaite passer un argument “bonjour” le retour attendu est “Bonjour” (execpt .toBe(“Bonjour”))
// it 3: Je souhaite passer un argument avec la valeur “” (vide) le retour attendu sera “Hello”(execpt .toBe(“Hello”))

// L’objectif ici est de produire du votre document puis le code associé avec les potentiels retours attendus. 
// Tous les cas que vous pourrez énumérer permettront de couvrir l'intégralité des fonctions présentées ci-dessous. 
// Utilisez jest et veillez à bien utiliser l’option --coverage pour déterminer si vous arrivez bien au 80% minimum 
// de couverture du code.


const my_alpha_number_t = nbr => `${nbr}`;

const sum = (a, b) => {
  if (typeof a != 'number' || typeof b != 'number' ) {
    return 0;
  }

  return a + b;
};

const my_size_alpha_t = (str = '') => {
  let count = 0;

  if (typeof str != 'string') {
    return count;
  }

  while(!!str[count]) {
    count++;
  }

  return count;
}

const my_display_alpha_t = () => 'abcdefghijklmnopqrstuvwxyz';

const my_array_alpha_t = (str) => {
  const result = [];

  for (let i = 0; i < my_size_alpha_t(str); i += 1) {
    result[i] = str[i];
  }

  return result;
};

const my_is_posi_neg_t = (nbr) => {
  if (nbr <= 0) {
    return 'NEGATIVE';
  }

  return 'POSITIF';
};

const fibo = (n) => {
  if (n <= 0) {
    return 0;
  }
 
  if (n == 1 || n == 2) {
   return 1;
  }

  return fibo(n - 1) + fibo(n - 2);
};

const my_display_alpha_reverse_t = () => {
  const alpha = my_display_alpha_t();
  let reverseAlpha = '';

  for (let i = my_size_alpha_t(alpha); i > 0; i -= 1) {
    reverseAlpha += alpha[i-1];
  }

  return reverseAlpha;
}

const my_length_array_t = (arr) => {
  let i = 0;

  while(!!arr[i]) {
    i += 1;
  }

  return i;
}

const my_display_unicode_t = (arr) => {
  const results = [];

  for (let i = 0; i < arr.length; i += 1) {
    const decimal = arr[i];

    if ((decimal >= 65 && decimal <= 99)) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if ((decimal >= 97 && decimal <= 122)) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if ((decimal >= 48 && decimal <= 57)) {
      results[i] = String.fromCharCode(arr[i]);
    }
    if (decimal === 32) {
      results[i] = String.fromCharCode(arr[i]);
    }
  }

  return results.join('');
};


export { my_alpha_number_t };
export { sum };
export { my_size_alpha_t };
export { my_display_alpha_t };
export { my_array_alpha_t };
export { my_is_posi_neg_t };
export { fibo };
export { my_display_alpha_reverse_t };
export { my_length_array_t };
export { my_display_unicode_t };