/*

*/

import { my_alpha_number_t } from "../src/index.js";
import { sum } from "../src/index.js";
import { my_size_alpha_t } from "../src/index.js";
import { my_display_alpha_t } from "../src/index.js";
import { my_array_alpha_t } from "../src/index.js";
import { my_is_posi_neg_t } from "../src/index.js";
import { fibo } from "../src/index.js";
import { my_display_alpha_reverse_t } from "../src/index.js"
import { my_length_array_t } from "../src/index.js";
import { my_display_unicode_t } from "../src/index.js";


describe('my_alpha_number_t function test', () => {
    it('Should convert a number to a string', () => {
        const result = my_alpha_number_t(1);
        expect(result).toBe('1');
    });
});



describe('sum function test', () => {
    it('Should add two numbers', () => {
        const result = sum(3, 5);
        expect(result).toBe(8);
    });

    it('Should handle non-number inputs', () => {
        var result1Int = sum('a', 5);
        expect(result1Int).toBe(0);

        result1Int = sum(3, 'b');
        expect(result1Int).toBe(0);

        const resultNoInt = sum('a', 'b');
        expect(resultNoInt).toBe(0);
    });
});



describe('my_size_alpha_t function test', () => {
    it('Should return the length of a string', () => {
        const result = my_size_alpha_t('hello');
        expect(result).toBe(5);
    });

    it('Should return 0 for no string typeof inputs', () => {
        const resultNonString = my_size_alpha_t(1998);
        expect(resultNonString).toBe(0);
    });

    it('Should return 0 for empty strings', () => {
        const resultEmptyString = my_size_alpha_t('');
        expect(resultEmptyString).toBe(0);
    });
});



describe('my_display_alpha_t function test', () => {
    it('Should return the alphabet', () => {
        const result = my_display_alpha_t();
        const tobe = 'abcdefghijklmnopqrstuvwxyz';
        expect(result).toBe(tobe);
    });
});



describe('my_array_alpha_t function test', () => {
    it('Should return each element of a string in an array', () => {
        const result = my_array_alpha_t('warren');
        const tobe = ['w', 'a', 'r', 'r', 'e', 'n'];
        expect(result).toEqual(tobe);
    });

    // case where the argument is not a string is not handled

    it('Should return an empty array for empty strings', () => {
        const resultEmpty = my_array_alpha_t('');
        expect(resultEmpty).toEqual([]);
    });
});



describe('my_is_posi_neg_t function test', () => {
    it('Should return POSITIF if the number is > 0', () => {
        const result = my_is_posi_neg_t(5);
        const tobe = 'POSITIF';
        expect(result).toEqual(tobe)
    });

    it('Should return NEGATIVE if the number is < 0', () => {
        const result = my_is_posi_neg_t(-5);
        const tobe = 'NEGATIVE';
        expect(result).toEqual(tobe)
    });

    it('Should return NEGATIVE if the number is = 0', () => {
        const result = my_is_posi_neg_t(0);
        const tobe = 'NEGATIVE';
        expect(result).toEqual(tobe)
    });
});



describe('fibo function test', () => {
    it('Should return 0 if the number is 0', () => {
        const result = fibo(0);
        expect(result).toEqual(0)
    });

    it('Should return 1 if the number is 1 or 2', () => {
        var result = fibo(1);
        expect(result).toEqual(1)

        result = fibo(2);
        expect(result).toEqual(1)
    });

    it('Should return 1 if the number is 1 or 2', () => {
        var result = fibo(5);
        expect(result).toEqual(5)
    });
});



describe('my_display_alpha_reverse_t function test', () => {
    it('Should return the alphabet in a reversed order', () => {
        const result = my_display_alpha_reverse_t();
        const tobe = 'zyxwvutsrqponmlkjihgfedcba';
        expect(result).toEqual(tobe)
    });
});



describe('my_length_array_t function test', () => {
    it('Should return the length of the array', () => {
        const result = my_length_array_t([1, 2, 3, 4]);
        expect(result).toBe(4);
    });

    it('Should return 0', () => {
        const result = my_length_array_t([0, 1, 2, 3, 4]);
        expect(result).toBe(0);
    });

    it('Should return 2', () => {
        const result = my_length_array_t([1, 2, 0, 3, 4]);
        expect(result).toBe(2);
    });

    it('Should return 0 for an empty array', () => {
        const resultNoElement = my_length_array_t([]);
        expect(resultNoElement).toBe(0);
    });
});



describe('my_display_unicode_t function test', () => {
    it('Should display characters for valid Unicode values', () => {
        const inputArray = [65, 97, 100, 47, 48, 32];
        const tobe = 'Aad0 ';
        const result = my_display_unicode_t(inputArray);
        expect(result).toBe(tobe);
    });
});

